<?php
include 'LEGTA/entete.php';
?>

<!-- Permet d'avoir des images dans des cadres rond -->
<style type="text/css">
    .img-card{
        border-radius: 50%;
        width: 200px;
    }
</style>

<br>
<br>

<div class="container-fluid" align="center">
	<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #54BA04; border-right: 5px solid #54BA04">
	<br>
	<h1 align="center" style="font-family: 'Gentium Book Basic'"><b>Demande de sortie</b></h1>
	<br>
	</div>
	<br>
	<h2 class="h3.b" align="left" style="font-family: Arial"><b>Choix du Centre :</b></h2>
</div>
<div class="container-fluid" align="center">
    <hr>
	
    <!-- Réalisation de trois cases pour faire le choix entre les 3 centres -->
    <div class="card-deck">
	
        <!-- Case pour LEGTA -->
        <div class="card bg-light mb-3" style="border-top: 5px solid #54BA04"; style="max-width: 18rem">
            <form class="form-horizontal" method="post" action="LEGTA/connexion.php">
                <div class="card-header" background-color="">
                    <h5 class="card-title" align="center"><strong>Lycée</strong></h5>
                </div>
                <br>
                <img class="img-card" src="images/LEGTA.png" height="220" alt="Card image cap">
                <div class="card-body">
                    <hr>
                    <div class="form-group">
                        <div class="col text-center">
                            <button type="submit" class="btn btn-primary">Connexion</button>
                        </div>
                    </div>
                    <hr>
                </div>
            </form>
        </div>

        <!-- Case pour CFA -->
        <div class="card bg-light mb-3" style="border-top: 5px solid #54BA04"; style="max-width: 18rem">
            <form class="form-horizontal" method="post">
                <div class="card-header" background-color="">
                    <h5 class="card-title" align="center"><strong>CFA</strong></h5>
                </div>
                <br>
                <img class="img-card" src="images/CFA.jpg" height="220" alt="Card image cap">
                <div class="card-body">
                    <hr>
                    <div class="form-group">
                        <div class="col text-center">
                            <button type="submit" class="btn btn-primary">Connexion</button>
                        </div>
                    </div>
                    <hr>
                </div>
            </form>
        </div>

        <!-- Case pour CFPPA -->
        <div class="card bg-light mb-3" style="border-top: 5px solid #54BA04"; style="max-width: 18rem">
            <form class="form-horizontal" method="post">
                <div class="card-header" background-color="">
                    <h5 class="card-title" align="center"><strong>CFPPA</strong></h5>
                </div>
                <br>
                <img class="img-card" src="images/CFPPA.jpg" height="220" alt="Card image cap">
                <div class="card-body">
                    <hr>
                    <div class="form-group">
                        <div class="col text-center">
                            <button type="submit" class="btn btn-primary">Connexion</button>
                        </div>
                    </div>
                    <hr>
                </div>
            </form>
        </div>
    </div>
</div>
<hr width="75%">
<footer class="bg-dark fixed-bottom">
	<div class="text-center">
		<a href="http://eap72.fr/resa/documentation.php"> Documentation Utilisateur</a>
	</div>
</footer>