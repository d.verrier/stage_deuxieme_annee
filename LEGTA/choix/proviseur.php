<?php
session_start();
include '../entete.php';
$conn = new PDO('mysql:host=db5000078384.hosting-data.io;port=3306;dbname=dbs73017','dbu252833','BDD@ST@g1@1rE2019');

if (isset($_SESSION['statut'])){
	if ($_SESSION['statut']==2) {
?>

		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #0000FF; border-right: 5px solid #0000FF">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Votre choix concernant la sortie sélectionnée :</h1>
				<br>
			</div>
			<div class="tab-pane fade card-body active show">
				<form class="form-horizontal" method="post" action="validationProviseur.php">
					<br>
					<h4>Si vous souhaitez valider la demande de sortie cliquez ici :</h4>
					<input type="hidden" name="validationProviseur" value="<?php echo $_POST['num_proviseur'];?>">
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
						</div>
					</div>
				</form>
				<hr style="border-color: blue">
				<form class="form-horizontal" method="post" action="precisionProviseur.php">
					<h5>Si vous souhaitez avoir plus de précision sur la sortie sélectionnée, veuillez remplir les précisions à apporter ci-dessous pour que le demandeur concerné modifie sa demande :</h5>
					<br>
					<input type="hidden" name="precisionProviseur" value="<?php echo $_POST['num_proviseur'];?>">
					<textarea class="form-control" id="precision" name="precision" rows="2" cols="50" placeholder="Précision pour la demande..." required></textarea>
					<br>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" name="valider" values="valider" class="btn btn-warning">Valider la précision</button>
						</div>
					</div>
				</form>
				<hr style="border-color: blue">
				<form class="form-horizontal" method="post" action="refusProviseur.php">
					<h5>Si vous souhaitez refuser la sortie sélectionnée, veuillez remplir le motif de refus ci-dessous :</h5>
					<br>
					<input type="hidden" name="refusProviseur" value="<?php echo $_POST['num_proviseur'];?>">
					<textarea class="form-control" id="refus" name="refus" rows="2" cols="50" placeholder="Motif de refus..." required></textarea>
					<br>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" name="valider" values="valider" class="btn btn-danger">Valider le motif de refus</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	
<?php
	}
}

//permet à l'utilisateur de se connecter

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connectĂ©, merci de cliquer sur le bouton ci-dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<!-- Permet d'afficher le message d'erreur en rouge et de le centré  -->

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	} 
</style>