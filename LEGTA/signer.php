<?php
session_start();
include 'entete.php';

//permet de se connecter à la base de données MySQL

$conn = new PDO('mysql:host=db5000078384.hosting-data.io;port=3306;dbname=dbs73017','dbu252833','BDD@ST@g1@1rE2019');

unset($_SESSION['signature']);
$_SESSION['signature'] = $_POST['num_signature'];

//permet de vérifier que l'utilisateur connecté à pour statut "Proviseur adjoint"

if ($_SESSION['statut']==2) {
?>
	<div class="container-fluid" align="center">
		<br>
		<br>
		<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #54BA04; border-right: 5px solid #54BA04">
			<br>
			<h1 style="font-family: 'Gentium Book Basic'">Signature :</h1>
			<br>
		</div>
		<div class="tab-content">
			<br>
			<div class="tab-pane fade active show" style="border-left: 7px solid #54BA04">
				<form class="form-horizontal" method="post" action = "signature.php">
					<div class="alert alert-secondary">
						<p>
						<div class="row">
							<div class="form-group col text-center">
								<label for="dateSignature"><b>Date de signature :</b></label>
								<input class="form-control" id="dateSignature" name="dateSignature" type="date" required />
							</div>
						</div>
						<hr style="border-color: green">
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php
}

//permet à l'utilisateur de se connecter

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connectĂ©, merci de cliquer sur le bouton ci-dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<!-- Permet d'afficher le message d'erreur en rouge et de le centré  -->

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	} 
</style>