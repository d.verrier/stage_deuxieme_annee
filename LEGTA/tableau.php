<?php
session_start();
include 'entete.php';

//permet de se connecter à la base de données MySQL

$conn = new PDO('mysql:host=db5000078384.hosting-data.io;port=3306;dbname=dbs73017','dbu252833','BDD@ST@g1@1rE2019');

//permet de vérifier que l'utilisateur est connecté

if (isset($_SESSION['statut'])){
	
	//permet de vérifier que l'utilisateur connecté à pour statut "enseignant" ou "non-enseignant"
	
	if ($_SESSION['statut']==1 || $_SESSION['statut']==6) {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #0000FF; border-right: 5px solid #0000FF">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Tableau récapitulatif de vos demandes de sortie :</h1>
				<br>
			</div>
			<br>
			
			<style>
				table{
					width: 180%;
				}
			</style>
			
			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show">
					<div class="table-responsive">
						<table id="tableau1" class="table table-striped table-sm">
							<thead>
								<tr>
									<th>Date de la sortie</th>
									<th>Lieu de destination</th>
									<th>Heure de départ</th>
									<th>Heure de retour</th>
									<th>Nombre d'apprenants</th>
									<th>PDF</th>
									<th>Proviseur Adjoint</th>
									<th>Vie Scolaire</th>
									<th>Economat</th>
									<th>Signature</th>
								</tr>
							</thead>							
							<tbody bgcolor="beige">
<?php

								//requête de sélection permettant à l'utilisateur de visualiser les demandes dont il est impliqué

								$resultat = $conn->query("SELECT DISTINCT num, dateSortie, destination, heureDepart, heureRetour, nbEleves, reservation, validationProviseur, validationVieScolaire, validationEconomat, refusProviseur, refusVieScolaire, refusEconomat, signature FROM sortie LEFT JOIN accompagne ON sortie.num=accompagne.sortie where (sortie.connecte='".$_SESSION['id']."') OR (sortie.responsable='".$_SESSION['id']."') OR (accompagne.utilisateur='".$_SESSION['id']."');");
								while($donnee = $resultat->fetch()){
									
									list($year, $month, $day) = explode("-", $donnee['dateSortie']);
									$tdateso = $day.'/'.$month.'/'.$year;
									
									list($year, $month, $day) = explode("-", $donnee['signature']);
									$tdatesi = $day.' '.$month.' '.$year;
?>
									<tr value="<?php echo $donnee['num'] ?>" <?php if(($donnee['validationProviseur'] == 1 && $donnee['validationVieScolaire'] == 1 && $donnee['validationEconomat'] == 1) || ($donnee['validationProviseur'] == 1 && $donnee['validationVieScolaire'] == 1 && empty($donnee['reservation']))){ echo "class='bg-success'";} else if(isset($donnee['refusProviseur']) || isset($donnee['refusVieScolaire']) || isset($donnee['refusEconomat'])){ echo "class='bg-danger'";} ?>>
										<th scope="row"><?php echo $tdateso; ?></th>
										<td><?php echo $donnee['destination']; ?></td>
										<td><?php echo $donnee['heureDepart']; ?></td>
										<td><?php echo $donnee['heureRetour']; ?></td>
										<td><?php echo $donnee['nbEleves']; ?></td>
										<td>
											<form class="form-horizontal" method="post" target="_blank" action="recuperation.php">
												<div class="form-group">
													<input type="hidden" name="num_pdf" value="<?php echo $donnee['num'];?>">
													<button type="submit" name="pdf" value="bouton" class="btn btn-warning">PDF</button>
												</div>
											</form>
										</td>
										<td>
<?php
											if($donnee['validationProviseur'] == 1){
?>
												<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
<?php
											}
											else if(isset($donnee['refusProviseur'])){
?>
												<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button>
<?php
											}
?>
										</td>
										<td>
<?php
											if($donnee['validationVieScolaire'] == 1){
?>
												<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
<?php
											}
											else if(isset($donnee['refusVieScolaire'])){
?>
												<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button>
<?php
											}
?>
										</td>
										<td>
<?php
											if($donnee['validationEconomat'] == 1){
?>
												<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
<?php
											}
											else if(isset($donnee['refusEconomat'])){
?>
												<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button>
<?php
											}
?>
										</td>
										<td><?php echo $tdatesi; ?></td>
									</tr>
<?php
								}
?>
							</tbody>
						</table>						
						<script>
							$(document).ready(function() {
								$('#tableau1').DataTable( {
									"language": {
										"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
									},
									"order": [[ 0, "desc" ]]
								} );
							} );
						</script>
					</div>
				</div>
			</div>
		</div>
<?php
	}
	
	//permet de vérifier que l'utilisateur connecté à pour statut "Proviseur Adjoint"
	
	else if ($_SESSION['statut']==2) {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #0000FF; border-right: 5px solid #0000FF">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Tableau récapitulatif de toutes les demandes de sortie :</h1>
				<br>
			</div>
			<br>
			
			<style>
				table{
					width: 180%;
				}
			</style>
			
			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show">
					<div class="table-responsive">
						<table id="tableau2" class="table table-striped table-sm">
							<thead>
								<tr>
									<th>Demandeur</th>
									<th>Date de la sortie</th>
									<th>Lieu de destination</th>
									<th>Heure de départ</th>
									<th>Heure de retour</th>
									<th>Nombre d'apprenants</th>
									<th>PDF</th>
									<th>Proviseur Adjoint</th>
									<th>Vie Scolaire</th>
									<th>Economat</th>
									<th>Signature</th>
								</tr>
							</thead>							
							<tbody bgcolor="beige">
<?php
								//requête de sélection permettant à l'utilisateur de visualiser toutes les demandes

								$resultat = $conn->query("SELECT * FROM sortie INNER JOIN utilisateur ON sortie.connecte = utilisateur.id INNER JOIN organise ON sortie.num = organise.sortie WHERE centre = 1;");
								while($donnee = $resultat->fetch()){
									list($year, $month, $day) = explode("-", $donnee['dateSortie']);
									$tdateso = $day.'/'.$month.'/'.$year;
?>
									<tr value="<?php echo $donnee['num'] ?>" <?php if(($donnee['validationProviseur'] == 1 && $donnee['validationVieScolaire'] == 1 && $donnee['validationEconomat'] == 1) || ($donnee['validationProviseur'] == 1 && $donnee['validationVieScolaire'] == 1 && empty($donnee['reservation']))){ echo "class='bg-success'";} else if(isset($donnee['refusProviseur']) || isset($donnee['refusVieScolaire']) || isset($donnee['refusEconomat'])){ echo "class='bg-danger'";} ?>>
										<th scope="row"><?php echo $donnee['prenom']," ",$donnee['nom']; ?></th>
										<td><?php echo $tdateso; ?></td>
										<td><?php echo $donnee['destination']; ?></td>
										<td><?php echo $donnee['heureDepart']; ?></td>
										<td><?php echo $donnee['heureRetour']; ?></td>
										<td><?php echo $donnee['nbEleves']; ?></td>
										<td>
											<form class="form-horizontal" method="post" target="_blank" action="recuperation.php">
												<div class="form-group">
													<input type="hidden" name="num_pdf" value="<?php echo $donnee['num'];?>">
													<button type="submit" name="pdf" value="bouton" class="btn btn-warning">PDF</button>
												</div>
											</form>
										</td>
										<td>
<?php
											if (empty($donnee['validationProviseur']) && empty($donnee['precisionProviseur']) && empty($donnee['refusProviseur'])){
?>
												<form class="form-horizontal" method="post" action="choix/proviseur.php">
													<div class="form-group">
														<input type="hidden" name="num_proviseur" value="<?php echo $donnee['num'];?>">
														<button type="submit" class="btn btn-info"><i class="far fa-hand-pointer" style="font-size: 15px"></i></button>
													</div>
												</form>
<?php
											}
											else {
												if($donnee['validationProviseur'] == 1){
?>
													<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
<?php
												}
												else if(isset($donnee['refusProviseur'])){
?>
													<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button>
<?php
												}
											}
?>
										</td>
										<td>
<?php
											if($donnee['validationVieScolaire'] == 1){
?>
												<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
<?php
											}
											else if(isset($donnee['refusVieScolaire'])){
?>
												<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button>
<?php
											}
?>
										</td>
										<td>
<?php
											if($donnee['validationEconomat'] == 1){
?>
												<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
<?php
											}
											else if(isset($donnee['refusEconomat'])){
?>
												<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button>
<?php
											}
?>
										</td>
										<td>
<?php
											if(isset($donnee['signature'])){
												echo $donnee['signature'];
											}
											else if($donnee['validationProviseur'] == 1){
?>
												<form class="form-horizontal" method="post" action="signer.php">
													<div class="form-group">
														<input type="hidden" name="num_signature" value="<?php echo $donnee['num'];?>">
														<button type="submit" class="btn btn-info"><i class="far fa-hand-pointer" style="font-size: 15px"></i></button>
													</div>
												</form>
<?php
											}
?>
										</td>
									</tr>
<?php
								}
?>
							</tbody>
						</table>						
						<script>
							$(document).ready(function() {
								$('#tableau2').DataTable( {
									"language": {
										"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
									},
									"order": [[ 7, "desc" ]]
								} );
							} );
						</script>
					</div>
				</div>
			</div>
		</div>
<?php
	}
	
	//permet de vérifier que l'utilisateur connecté à pour statut "Vie Scolaire"
	
	else if ($_SESSION['statut']==3) {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #0000FF; border-right: 5px solid #0000FF">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Tableau récapitulatif des demandes de sortie :</h1>
				<br>
			</div>
			<br>
			
			<style>
				table{
					width: 180%;
				}
			</style>
			
			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show">
					<div class="table-responsive">
						<table id="tableau3" class="table table-striped table-sm">
							<thead>
								<tr>
									<th>Demandeur</th>
									<th>Date de la sortie</th>
									<th>Lieu de destination</th>
									<th>Heure de départ</th>
									<th>Heure de retour</th>
									<th>Nombre d'apprenants</th>
									<th>PDF</th>
									<th>Proviseur Adjoint</th>
									<th>Vie Scolaire</th>
									<th>Economat</th>
									<th>Signature</th>
								</tr>
							</thead>							
							<tbody bgcolor="beige">
<?php
								//requête de sélection permettant à l'utilisateur de visualiser toutes les demandes

								$resultat = $conn->query("SELECT * FROM sortie INNER JOIN utilisateur ON sortie.connecte = utilisateur.id INNER JOIN organise ON sortie.num = organise.sortie WHERE centre = 1 AND validationProviseur = 1 AND ((reservation IS NULL) OR (reservation = 1 AND validationEconomat = 1));");
								while($donnee = $resultat->fetch()){
									
									list($year, $month, $day) = explode("-", $donnee['dateSortie']);
									$tdateso = $day.'/'.$month.'/'.$year;
									
									list($year, $month, $day) = explode("-", $donnee['signature']);
									$tdatesi = $day.' '.$month.' '.$year;
?>
									<tr value="<?php echo $donnee['num'] ?>" <?php if(($donnee['validationProviseur'] == 1 && $donnee['validationVieScolaire'] == 1 && $donnee['validationEconomat'] == 1) || ($donnee['validationProviseur'] == 1 && $donnee['validationVieScolaire'] == 1 && empty($donnee['reservation']))){ echo "class='bg-success'";} else if(isset($donnee['refusProviseur']) || isset($donnee['refusVieScolaire']) || isset($donnee['refusEconomat'])){ echo "class='bg-danger'";} ?>>
										<th scope="row"><?php echo $donnee['prenom']," ",$donnee['nom']; ?></th>
										<td><?php echo $tdateso; ?></td>
										<td><?php echo $donnee['destination']; ?></td>
										<td><?php echo $donnee['heureDepart']; ?></td>
										<td><?php echo $donnee['heureRetour']; ?></td>
										<td><?php echo $donnee['nbEleves']; ?></td>
										<td>
											<form class="form-horizontal" method="post" target="_blank" action="recuperation.php">
												<div class="form-group">
													<input type="hidden" name="num_pdf" value="<?php echo $donnee['num'];?>">
													<button type="submit" name="pdf" value="bouton" class="btn btn-warning">PDF</button>
												</div>
											</form>
										</td>
										<td>
											<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
										</td>
										<td>
<?php
											if ($donnee['validationProviseur'] == 1 && empty($donnee['validationVieScolaire']) && empty($donnee['precisionVieScolaire']) && empty($donnee['refusVieScolaire']) && (empty($donnee['reservation']) || ($donnee['validationEconomat'] == 1 && $donnee['reservation'] == 1 ))){
?>
												<form class="form-horizontal" method="post" action="choix/vieScolaire.php">
													<div class="form-group">
														<input type="hidden" name="num_vie_scolaire" value="<?php echo $donnee['num'];?>">
														<button type="submit" class="btn btn-info"><i class="far fa-hand-pointer" style="font-size: 15px"></i></button>
													</div>
												</form>
<?php
											}
											else {
												if($donnee['validationVieScolaire'] == 1){
?>
													<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
<?php
												}
												else if(isset($donnee['refusVieScolaire'])){
?>
													<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button>
<?php
												}
											}
?>
										</td>
										<td>
<?php
											if($donnee['validationEconomat'] == 1){
?>
												<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
<?php
											}
											else if(isset($donnee['refusEconomat'])){
?>
												<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button>
<?php
											}
?>
										</td>
										<td><?php echo $tdatesi; ?></td>
									</tr>
<?php
								}
?>
							</tbody>
						</table>						
						<script>
							$(document).ready(function() {
								$('#tableau3').DataTable( {
									"language": {
										"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
									},
									"order": [[ 8, "desc" ]]
								} );
							} );
						</script>
					</div>
				</div>
			</div>
		</div>
<?php
	}
	
	//permet de vérifier que l'utilisateur connecté à pour statut "Economat"
	
	else if ($_SESSION['statut']==4) {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #0000FF; border-right: 5px solid #0000FF">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Tableau récapitulatif des demandes de sortie:</h1>
				<br>
			</div>
			<br>
			
			<style>
				table{
					width: 180%;
				}
			</style>
			
			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show">
					<div class="table-responsive">
						<table id="tableau4" class="table table-striped table-sm">
							<thead>
								<tr>
									<th>Demandeur</th>
									<th>Date de la sortie</th>
									<th>Lieu de destination</th>
									<th>Heure de départ</th>
									<th>Heure de retour</th>
									<th>Nombre d'apprenants</th>
									<th>PDF</th>
									<th>Proviseur Adjoint</th>
									<th>Vie Scolaire</th>
									<th>Economat</th>
									<th>Signature</th>
								</tr>
							</thead>							
							<tbody bgcolor="beige">
<?php
								//requête de sélection permettant à l'utilisateur de visualiser les demandes dont il y a une reservation de repas

								$resultat = $conn->query("SELECT * FROM sortie INNER JOIN utilisateur ON sortie.connecte = utilisateur.id INNER JOIN organise ON sortie.num = organise.sortie WHERE centre = 1 AND validationProviseur = 1;");
								while($donnee = $resultat->fetch()){
									
									list($year, $month, $day) = explode("-", $donnee['dateSortie']);
									$tdateso = $day.'/'.$month.'/'.$year;
									
									list($year, $month, $day) = explode("-", $donnee['signature']);
									$tdatesi = $day.' '.$month.' '.$year;
?>
									<tr value="<?php echo $donnee['num'] ?>" <?php if(($donnee['validationProviseur'] == 1 && $donnee['validationVieScolaire'] == 1 && $donnee['validationEconomat'] == 1) || ($donnee['validationProviseur'] == 1 && $donnee['validationVieScolaire'] == 1 && empty($donnee['reservation']))){ echo "class='bg-success'";} else if(isset($donnee['refusProviseur']) || isset($donnee['refusVieScolaire']) || isset($donnee['refusEconomat'])){ echo "class='bg-danger'";} ?>>
										<th scope="row"><?php echo $donnee['prenom']," ",$donnee['nom']; ?></th>
										<td><?php echo $tdateso; ?></td>
										<td><?php echo $donnee['destination']; ?></td>
										<td><?php echo $donnee['heureDepart']; ?></td>
										<td><?php echo $donnee['heureRetour']; ?></td>
										<td><?php echo $donnee['nbEleves']; ?></td>
										<td>
											<form class="form-horizontal" method="post" target="_blank" action="recuperation.php">
												<div class="form-group">
													<input type="hidden" name="num_pdf" value="<?php echo $donnee['num'];?>">
													<button type="submit" name="pdf" value="bouton" class="btn btn-warning">PDF</button>
												</div>
											</form>
										</td>
										<td>
											<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
										</td>
										<td>
<?php
											if($donnee['validationVieScolaire'] == 1){
?>
												<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
<?php
											}
											else if(isset($donnee['refusVieScolaire'])){
?>
												<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button>
<?php
											}
?>
										</td>
										<td>
<?php
											if ($donnee['validationProviseur'] == 1 && $donnee['reservation'] == 1 && empty($donnee['validationEconomat']) && empty($donnee['precisionEconomat']) && empty($donnee['refusEconomat'])){
?>
												<form class="form-horizontal" method="post" action="choix/economat.php">
													<div class="form-group">
														<input type="hidden" name="num_economat" value="<?php echo $donnee['num'];?>">
														<button type="submit" class="btn btn-info"><i class="far fa-hand-pointer" style="font-size: 15px"></i></button>
													</div>
												</form>
<?php
											}
											else {
												if($donnee['validationEconomat'] == 1){
?>
													<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
<?php
												}
												else if(isset($donnee['refusEconomat'])){
?>
													<button class="btn btn-danger"><i class="far fa-times-circle" style="font-size: 15px"></i></button>
<?php
												}
											}
?>
										</td>
										<td><?php echo $tdatesi; ?></td>
									</tr>
<?php
								}
?>
							</tbody>
						</table>						
						<script>
							$(document).ready(function() {
								$('#tableau4').DataTable( {
									"language": {
										"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
									},
									"order": [[ 9, "desc" ]]
								} );
							} );
						</script>
					</div>
				</div>
			</div>
		</div>
<?php
	}
	
	//permet de vérifier que l'utilisateur connecté à pour statut "Invité"
	
	else if ($_SESSION['statut']==5) {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #0000FF; border-right: 5px solid #0000FF">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Tableau récapitulatif de vos demandes de sortie :</h1>
				<br>
			</div>
			<br>
			
			<style>
				table{
					width: 180%;
				}
			</style>
			
			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show">
					<div class="table-responsive">
						<table id="tableau1" class="table table-striped table-sm">
							<thead>
								<tr>
									<th>Date de la sortie</th>
									<th>Lieu de destination</th>
									<th>Heure de départ</th>
									<th>Heure de retour</th>
									<th>Nombre d'apprenants</th>
									<th>PDF</th>
									<th>Proviseur Adjoint</th>
									<th>Vie Scolaire</th>
									<th>Economat</th>
									<th>Signature</th>
								</tr>
							</thead>							
							<tbody bgcolor="beige">
<?php
								//requête de sélection permettant à l'utilisateur de visualiser toutes les demandes validées
								
								$resultat = $conn->query("SELECT * FROM `sortie` WHERE (validationProviseur=1 AND validationVieScolaire=1 AND validationEconomat=1) OR (validationProviseur=1 AND validationVieScolaire=1 AND reservation IS NULL);");
								while($donnee = $resultat->fetch()){
									
									list($year, $month, $day) = explode("-", $donnee['dateSortie']);
									$tdateso = $day.'/'.$month.'/'.$year;
									
									list($year, $month, $day) = explode("-", $donnee['signature']);
									$tdatesi = $day.' '.$month.' '.$year;
?>
									<tr value="<?php echo $donnee['num'] ?>" <?php if(($donnee['validationProviseur'] == 1 && $donnee['validationVieScolaire'] == 1 && $donnee['validationEconomat'] == 1) || ($donnee['validationProviseur'] == 1 && $donnee['validationVieScolaire'] == 1 && empty($donnee['reservation']))){ echo "class='bg-success'";} else if(isset($donnee['refusProviseur']) || isset($donnee['refusVieScolaire']) || isset($donnee['refusEconomat'])){ echo "class='bg-danger'";} ?>>
										<th scope="row"><?php echo $tdateso; ?></th>
										<td><?php echo $donnee['destination']; ?></td>
										<td><?php echo $donnee['heureDepart']; ?></td>
										<td><?php echo $donnee['heureRetour']; ?></td>
										<td><?php echo $donnee['nbEleves']; ?></td>
										<td>
											<form class="form-horizontal" method="post" target="_blank" action="recuperation.php">
												<div class="form-group">
													<input type="hidden" name="num_pdf" value="<?php echo $donnee['num'];?>">
													<button type="submit" name="pdf" value="bouton" class="btn btn-warning">PDF</button>
												</div>
											</form>
										</td>
										<td>
											<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
										</td>
										<td>
											<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
										</td>
										<td>
<?php
											if($donnee['validationEconomat'] == 1){
?>
												<button class="btn btn-success"><i class="far fa-check-circle" style="font-size: 15px"></i></button>
<?php
											}
?>
										</td>
										<td><?php echo $tdatesi; ?></td>
									</tr>
<?php
								}
?>
							</tbody>
						</table>						
						<script>
							$(document).ready(function() {
								$('#tableau1').DataTable( {
									"language": {
										"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
									},
									"order": [[ 0, "desc" ]]
								} );
							} );
						</script>
					</div>
				</div>
			</div>
		</div>
<?php
	}
}

//permet à l'utilisateur de se connecter

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connectĂ©, merci de cliquer sur le bouton ci-dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<!-- Permet d'afficher le message d'erreur en rouge et de le centré  -->

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	} 
</style>