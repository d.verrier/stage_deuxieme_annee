<?php
session_start();
require __DIR__.'/../vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;

  ob_start();
  ?>
<page> 
	<br/>
	<h2 style="text-align: center;">Autorisation de déplacement : </h2>
	<hr/>
	<h4 style="text-align: center;">Demandeur : </h4>
	<table style="vertical-align: top;">
        <tr>
            <td>
				<strong>Date de la demande : </strong><?php echo $_SESSION['pdfdateDemande'] ?><br /><br />
                <strong>Prénom : </strong><?php echo $_SESSION['pdfconnecteprenom'] ?><br /><br />
                <strong>Téléphone : </strong> <?php echo $_SESSION['pdfconnectetelephone'] ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            </td>
            <td>
				<strong>Statut : </strong><?php echo $_SESSION['pdfconnectestatut'] ?><br /><br />
                <strong>Nom : </strong><?php echo $_SESSION['pdfconnectenom'] ?><br /><br />
                <strong>E-mail : </strong><?php echo $_SESSION['pdfconnectemail'] ?>
            </td>
        </tr>
    </table>
	<hr/>
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	<strong>Résidence administrative : </strong>EPLEFPA La Germinière 72700 ROUILLON
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	<strong>Centre : </strong>LEGTA
	<hr/>
	<h4 style="text-align: center;">Sortie : </h4>
	<p><strong>Nature de la mission : </strong><?php echo $_SESSION['pdfnature'] ?></p>
	<table style="vertical-align: top;">
        <tr>
            <td>
                <strong>Date de sortie : </strong><?php echo $_SESSION['pdfdateSortie'] ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            </td>
            <td>
                <strong>Heure de départ : </strong><?php echo $_SESSION['pdfheureDepart'] ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            </td>
			<td>
                <strong>Heure de retour : </strong><?php echo $_SESSION['pdfheureRetour'] ?>
            </td>
        </tr>
    </table>
	<p><strong>Destination : </strong><?php echo $_SESSION['pdfdestination'] ?></p>
	<p><strong>Nombre total d'apprenants : </strong><?php echo $_SESSION['pdfnbEleves'] ?></p>
	<table style="vertical-align: top;">
        <tr>
            <td>
                <strong>Nombre de kilomètres A/R : </strong> <?php echo $_SESSION['pdfnbKm'] ?>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            </td>
            <td>
                <strong>Coût estimatif du bus exterieur : </strong><?php echo $_SESSION['pdfcoutEstimatif'] ?> €
            </td>
        </tr>
    </table>
	<p><strong>Lien avec le référentiel : </strong><?php echo $_SESSION['pdflienReferentiel'] ?></p>
	<hr/>
	<p style="text-align: center;"><strong>Véhicule(s) emprunté(s) (consulter GRR pour les disponibilités)</strong></p>
	<table style="vertical-align: top;">
		<tr>
			<td>
				<?php
				for ($v=0 ; $v < $_SESSION['pdfvehicule'] ;$v++){
					?>
					<?php
					echo $_SESSION['pdfvehiculeimmatriculation'][$v];
					?>
					-<strong>
					<?php
					echo $_SESSION['pdfvehiculemodele'][$v];
					?>
					</strong>&nbsp; &nbsp; &nbsp; &nbsp; 
					<?php
				}
				?>
			</td>
		</tr>
	</table>
	<hr/>
	<p style="text-align: center;"><strong>Bus SETRAM :</strong></p>
	<p><strong>Nombre de tickets à prévoir : </strong><?php echo $_SESSION['pdfnbTicketSetram'] ?></p>
	<hr/>
	<p style="text-align: center;"><strong>Classe(s) ou groupe(s) :</strong></p>
	<table style="vertical-align: top;">
		<tr>
			<td>
				<?php
				for ($c=0 ; $c < $_SESSION['pdfclasse'] ;$c++){
					echo $_SESSION['pdfclasselibelle'][$c];
					?>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<?php
				}
				?>
			</td>
		</tr>
	</table>
	<br/>
	<table>
        <thead>
            <tr>
                <th style="border: 1px solid #000;" "background: #000;" "font-size: 14px;" "text-align: center;">Autre centre &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
                <th style="border: 1px solid #000;" "background: #000;" "font-size: 14px;" "text-align: center;">Classe concernée &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
                <th style="border: 1px solid #000;" "background: #000;" "font-size: 14px;" "text-align: center;">Nombre d'apprenants &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
            </tr>
        </thead>
		<tbody>
			<?php 
			for ($g=0 ; $g < $_SESSION['pdfinvite'] ;$g++){
				?>
				<tr>
					<td style="border: 1px solid #CFD1D2;" "text-align: center;"><?php echo $_SESSION['pdfinvitecentre'][$g]; ?></td>
					<td style="border: 1px solid #CFD1D2;" "text-align: center;"><?php echo $_SESSION['pdfinviteclasse'][$g]; ?></td>
					<td style="border: 1px solid #CFD1D2;" "text-align: center;"><?php echo $_SESSION['pdfinvitenombre'][$g]; ?></td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>
	<hr/>
	<h4 style="text-align: center;">Responsable : </h4>
	<p><strong>Statut : </strong><?php echo $_SESSION['pdfresponsablestatut'] ?></p>
	<table style="vertical-align: top;">
        <tr>
            <td>
                <strong>Prénom : </strong><?php echo $_SESSION['pdfresponsableprenom'] ?><br /><br />
                <strong>Téléphone : </strong> <?php echo $_SESSION['pdfresponsabletelephone'] ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            </td>
            <td>
                <strong>Nom : </strong><?php echo $_SESSION['pdfresponsablenom'] ?><br /><br />
                <strong>E-mail : </strong><?php echo $_SESSION['pdfresponsablemail'] ?>
            </td>
        </tr>
    </table>
	<hr/>
	<p style="text-align: center;"><strong>Accompagnateur(s) :</strong></p>
	<table style="vertical-align: top;">
		<tr>
			<td>
				<?php
				for ($u=0 ; $u < $_SESSION['pdfutilisateur'] ;$u++){
					?>
					<?php
					echo $_SESSION['pdfutilisateurprenom'][$u];
					?>
					&nbsp;
					<?php
					echo $_SESSION['pdfutilisateurnom'][$u];
					?>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<?php
				}
				?>
			</td>
		</tr>
	</table>
	<hr/>
	<h4 style="text-align: center;">Remplacement : </h4>
	<table>
        <thead>
            <tr>
                <th style="border: 1px solid #000;" "background: #000;" "font-size: 14px;" "text-align: center;">Classe concernée &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
                <th style="border: 1px solid #000;" "background: #000;" "font-size: 14px;" "text-align: center;">Cours concerné &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
                <th style="border: 1px solid #000;" "background: #000;" "font-size: 14px;" "text-align: center;">Remplacement proposé &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
            </tr>
        </thead>
		<tbody>
			<?php 
			for ($r=0 ; $r < $_SESSION['pdfremplacement'] ;$r++){
				?>
				<tr>
					<td style="border: 1px solid #CFD1D2;" "text-align: center;"><?php echo $_SESSION['pdfremplacementlibelle'][$r]; ?></td>
					<td style="border: 1px solid #CFD1D2;" "text-align: center;"><?php echo $_SESSION['pdfremplacementcours'][$r]; ?></td>
					<td style="border: 1px solid #CFD1D2;" "text-align: center;"><?php echo $_SESSION['pdfremplacementproposition'][$r]; ?></td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>
	<hr/>
	<h4 style="text-align: center;">Repas : </h4>
	<?php
	
	//permet de vérifier l'existence d'un repas
	
	if (!empty($_SESSION['pdftype'])){
		if (!empty($_SESSION['pdfvegetarien'])){
		?>
			<table style="vertical-align: top;">
				<tr>
					<td>
						<strong>Type du repas : </strong> <?php echo $_SESSION['pdftype'] ?><br /><br />
						<strong>Repas végétarien : </strong> <?php echo $_SESSION['pdfvegetarien'] ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					</td>
					<td>
						<strong>Heure d'enlèvement en cuisine : </strong><?php echo $_SESSION['pdfheureEnlevement'] ?><br /><br />
						<strong>Nombre de repas végétarien : </strong> <?php echo $_SESSION['pdfnbVegetarien'] ?>
					</td>
				</tr>
			</table>
		<?php
		}
		else {
		?>
			<table style="vertical-align: top;">
				<tr>
					<td>
						<strong>Type du repas : </strong> <?php echo $_SESSION['pdftype'] ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					</td>
					<td>
						<strong>Heure d'enlèvement en cuisine : </strong><?php echo $_SESSION['pdfheureEnlevement'] ?>
					</td>
				</tr>
			</table>
		<?php
		}
		?>
		<br/>
		<br/>
		<p style="text-align: center;"><strong>Nombre total de repas à réserver : </strong></p>
		<table style="vertical-align: top;">
			<tr>
				<td>
					<strong>Interne(s) : </strong><?php echo $_SESSION['pdfnbInterne'] ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				</td>
				<td>
					<strong>Demi-pensionnaire(s) : </strong><?php echo $_SESSION['pdfnbDP'] ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				</td>
				<td>
					<strong>Externe(s) : </strong><?php echo $_SESSION['pdfnbExterne'] ?>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				</td>
				<td>
					<strong>Accompagnateur(s) : </strong><?php echo $_SESSION['pdfnbAccompagnateurs'] ?>
				</td>
			</tr>
		</table>
		<?php
	}
	else {
		?>
		<p><strong>Type du repas : </strong>
		<?php
		echo "Pas de repas";
		?>
		</p>
		<?php
	}
	?>
	<hr/>
	<br/>
	<p>Signature du Directeur de l'EPLEFPA ou du Directeur du Centre :</p>
</page>
	<?php
	$content = ob_get_clean();
$html2pdf = new Html2Pdf();
$html2pdf->writeHTML($content);
$html2pdf->output('sortie.pdf');
?>