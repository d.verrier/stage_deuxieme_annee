<?php
session_start();
include 'entete.php';

//Page d'accueil pour les utilisateurs connectés

if (isset($_SESSION['statut'])){
	
	//Page d'accueil pour les utilisateurs ayant le statut administrateur
	
	if ($_SESSION['statut']==7) {
		unset($_SESSION['demande_modification']);
		unset($_SESSION['existea']);
		unset($_SESSION['existem']);
?>
		<div class="container-fluid">
			<br>
			<br>
			<hr>
			<h1 style="text-align: center"><b>LEGTA</b></h1>
			<hr>
			<div class="row bg-light">
				<div class="col-sm" align="center">
					<a href="http://eap72.fr/resa/LEGTA/compte/tableau.php"><i class="fas fa-clipboard-list" style="font-size:100px"></i></a>
					<br>
					<hr>
					<p><b>Tableau Récapitulatif</b></p>
				</div>
				<!------------------------------------------------------------------------------------------------------------------------------------->
				<div class="col-sm" align="center">
					<a href="http://eap72.fr/resa/LEGTA/compte/ajouter.php"><i class="fas fa-plus-circle" style="font-size:100px"></i></a>
					<br>
					<hr>
					<p><b>Ajouter un compte</b></p>
				</div>
				<!------------------------------------------------------------------------------------------------------------------------------------->
				<div class="col-sm" align="center">
					<a href="http://eap72.fr/resa/LEGTA/compte/modifier.php"><i class="fas fa-edit" style="font-size:100px"></i></a>
					<br>
					<hr>
					<p><b>Modifier un compte</b></p>
				</div>
				<!------------------------------------------------------------------------------------------------------------------------------------->
				<div class="col-sm" align="center">
					<a href="http://eap72.fr/resa/LEGTA/compte/annuler.php"><i class="fas fa-trash-alt" style="font-size: 100px"></i></a>
					<br>
					<hr>
					<p><b>Supprimer un compte</b></p>
				</div>
			</div>
		</div>
		<hr>
<?php
	}
}

//Page d'accueil pour les utilisateurs non-connectés

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connecté, merci de cliquer sur le bouton si dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" name="centre01" values="centre01" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	} 
</style>