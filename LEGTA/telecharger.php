<?php
include 'entete.php';

try {$bdd = new PDO('mysql:host=db5000078384.hosting-data.io;port=3306;dbname=dbs73017','dbu252833','BDD@ST@g1@1rE2019');}
catch(Exception $e) { exit('<b>Catched exception at line '. $e->getLine() .' :</b> '. $e->getMessage());}

$sql = $bdd->prepare("SELECT sortie.num, destination, nature, dateSortie, heureDepart, heureRetour, nbKm, coutEstimatif, lienReferentiel, nbTicketSetram, nbEleves, dateDemande, prenom, nom, type, vegetarien, nbVegetarien, nbInterne, nbDP, nbExterne, nbAccompagnateurs, heureEnlevement FROM sortie LEFT JOIN repas ON sortie.num=repas.sortie INNER JOIN utilisateur ON sortie.connecte = utilisateur.id INNER JOIN organise ON sortie.num = organise.sortie WHERE centre = 1 AND (dateSortie BETWEEN '".$_POST['dateDebut']."' AND '".$_POST['dateFin']."') AND ((validationProviseur =1 AND validationVieScolaire = 1 AND validationEconomat = 1) OR (validationProviseur =1 AND validationVieScolaire = 1 AND reservation IS NULL));");
$sql -> execute(array());
 
while ($ligneencours = $sql -> fetch(PDO::FETCH_ASSOC))
{
	$num = $ligneencours["num"];
	$sql2 = $bdd->prepare("SELECT libelle FROM sortie INNER JOIN concerne ON concerne.sortie = sortie.num INNER JOIN classe ON concerne.classe = classe.num INNER JOIN organise ON sortie.num = organise.sortie WHERE sortie.num = '" . $num . "' AND centre = 1 AND (dateSortie BETWEEN '".$_POST['dateDebut']."' AND '".$_POST['dateFin']."') AND ((validationProviseur =1 AND validationVieScolaire = 1 AND validationEconomat = 1) OR (validationProviseur =1 AND validationVieScolaire = 1 AND reservation IS NULL));");
	$sql2 -> execute(array());
 
	while ($ligneencours2 = $sql2 -> fetch(PDO::FETCH_ASSOC))
	{
		$export[] = array($ligneencours2["libelle"], $ligneencours["destination"], $ligneencours["nature"], $ligneencours["dateSortie"], $ligneencours["heureDepart"], $ligneencours["heureRetour"], $ligneencours["nbKm"], $ligneencours["coutEstimatif"], $ligneencours["lienReferentiel"], $ligneencours["nbTicketSetram"], $ligneencours["nbEleves"], $ligneencours["dateDemande"], $ligneencours["prenom"], $ligneencours["nom"], $ligneencours["type"], $ligneencours["vegetarien"], $ligneencours["nbVegetarien"], $ligneencours["nbInterne"], $ligneencours["nbDP"], $ligneencours["nbExterne"], $ligneencours["nbAccompagnateurs"], $ligneencours["heureEnlevement"]);
	}
}

$chemin = 'sortie.csv';
$delimiteur = ';';

$fichier_csv = fopen($chemin, 'w+');
fprintf($fichier_csv, chr(0xEF).chr(0xBB).chr(0xBF));

$entetes = array('Classe', 'Destination', 'Nature', 'Date de sortie', 'Heure de départ', 'Heure de retour', 'Kilometres', 'Cout estimatif', 'Lien referentiel', 'Tickets Setram', 'Apprenants', 'Date de la demande', 'Prenom du demandeur', 'Nom du demandeur', 'Type de repas', 'Repas vegetarien', 'Vegetarien', 'Internes', 'Demi-pensionnaires', 'Externes', 'Accompagnateurs', 'Heure enlevement en cuisine');
fputcsv($fichier_csv, $entetes, $delimiteur);

foreach($export as $ligneaexporter){
    fputcsv($fichier_csv, $ligneaexporter, $delimiteur);
}

fclose($fichier_csv);

list($year, $month, $day) = explode("-", $_POST['dateDebut']);
$cdated = $day.'/'.$month.'/'.$year;

list($year, $month, $day) = explode("-", $_POST['dateFin']);
$cdatef = $day.'/'.$month.'/'.$year;
?>

<div class="container-fluid">
	<br>
	<br>
	<hr>
	<h1 style="text-align: center"><b>Tableau au format CSV</b></h1>
	<hr>
	<div class="row bg-light">
		<div class="col-sm" align="center">
			<a href='sortie.csv' target='_blank'><i class="fas fa-file-csv" style="font-size:100px"></i></a>
			<br>
			<hr>
			<p><b>Télécharger pour la période du <?php echo $cdated, ' au ' ,$cdatef ?></b></p>
		</div>
	</div>
</div>
<hr>