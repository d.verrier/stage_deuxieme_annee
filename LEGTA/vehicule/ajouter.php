<?php
session_start();
include '../entete.php';
$conn = new PDO('mysql:host=db5000078384.hosting-data.io;port=3306;dbname=dbs73017','dbu252833','BDD@ST@g1@1rE2019');
if ($_SESSION['statut']==7) {
	if(isset($_SESSION['existeav']) && $_SESSION['existeav']==1) {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #54BA04; border-right: 5px solid #54BA04">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Nouveau vehicule :</h1>
				<br>
			</div>
			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show" style="border-left: 7px solid #54BA04">
					<form class="form-horizontal" method="post" action = "ajout.php">
						<div class="alert alert-secondary">
							<br>
							<h3 style="color: red;" align="center">Attention : L'immatriculation saisie existe déja ! </h3>
							<br>
							<hr style="border-color: green">
							<p>
							<div class="row" >
								<div class="form-group col text-center">
									<label for="immatriculation"><b>Immatriculation :</b></label>
									<input class="form-control" id="immatriculation" name="immatriculation" type="text" value="<?php echo $_SESSION['immatriculationEx'] ?>" required />
								</div>
							</div>
							<hr style="border-color: green">
							<div class="row">
								<div class="form-group col text-center">
									<label for="marque"><b>Marque :</b></label>
									<input class="form-control" id="marque" name="marque" type="text" value="<?php echo $_SESSION['marqueEx'] ?>" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="modele"><b>Modele :</b></label>
									<input class="form-control" id="modele" name="modele" type="text" value="<?php echo $_SESSION['modeleEx'] ?>" required />
								</div>
							</div>
							<hr style="border-color: green">
							<div class="row">
								<div class="form-group col text-center">
									<label for="nbPlaces"><b>Nombre de places :</b></label>
									<input class="form-control" id="nbPlaces" name="nbPlaces" type="number" min="0" value="<?php echo $_SESSION['nbPlacesEx'] ?>" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="info"><b>Informations :</b></label>
									<input class="form-control" id="info" name="info" type="text" value="<?php echo $_SESSION['infoEx'] ?>" />
								</div>
							</div>
							<hr style="border-color: green">
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
<?php
	}
	else {
?>
		<div class="container-fluid" align="center">
			<br>
			<br>
			<div class="card bg-light mb-4"  style="max-width: 50%;border-left: 5px solid #54BA04; border-right: 5px solid #54BA04">
				<br>
				<h1 style="font-family: 'Gentium Book Basic'">Nouveau vehicule :</h1>
				<br>
			</div>
			<div class="tab-content">
				<br>
				<div class="tab-pane fade active show" style="border-left: 7px solid #54BA04">
					<form class="form-horizontal" method="post" action = "ajout.php">
						<div class="alert alert-secondary">
							<p>
							<div class="row" >
								<div class="form-group col text-center">
									<label for="immatriculation"><b>Immatriculation :</b></label>
									<input class="form-control" id="immatriculation" name="immatriculation" type="text" placeholder="XE 563 XE" required />
								</div>
							</div>
							<hr style="border-color: green">
							<div class="row">
								<div class="form-group col text-center">
									<label for="marque"><b>Marque :</b></label>
									<input class="form-control" id="marque" name="marque" type="text" placeholder="Renault" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="modele"><b>Modele :</b></label>
									<input class="form-control" id="modele" name="modele" type="text" placeholder="Clio Estate" required />
								</div>
							</div>
							<hr style="border-color: green">
							<div class="row">
								<div class="form-group col text-center">
									<label for="nbPlaces"><b>Nombre de places :</b></label>
									<input class="form-control" id="nbPlaces" name="nbPlaces" type="number" min="0" placeholder="8" required />
								</div>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								<div class="form-group col text-center">
									<label for="info"><b>Informations :</b></label>
									<input class="form-control" id="info" name="info" type="text" placeholder="Attelage"/>
								</div>
							</div>
							<hr style="border-color: green">
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" name="valider" values="valider" class="btn btn-success">Valider</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
<?php
	}
}

else {
?>
	<br>
	<div class="erreur">Vous n'etes pas connectĂ©, merci de cliquer sur le bouton ci-dessous pour vous connecter</div>
	<br>
	<form class="form-horizontal" method="post" action="../connexion.php">
		<div class="form-group">
			<div class="col text-center">
				<button type="submit" class="btn btn-primary">Se connecter</button>
			</div>
		</div>
	</form>
<?php
}
?>

<style type="text/css">
	.erreur{
		text-align: center;
		color : red;
	} 
</style>